package it.unict;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;  

public class ReportDidScriptlet extends JRDefaultScriptlet {
	
	Double valueFromParam = 0.0;
	Double fahrenheit = 0.0;
	MessageDigest md5;

    public String hello() throws JRScriptletException
    {
        return "Hello! I'm the report's scriptlet object.";
    }
    
    public void afterReportInit() throws JRScriptletException {
//    	valueFromParam = (Double)this.getParameterValue("celsius");
    	try {
			md5 = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// TODO Blocco catch generato automaticamente
			e.printStackTrace();
		}
    }
    

//    @Override
//    public void afterDetailEval() throws JRScriptletException {    
//        setVariableValue("gmean", stats.getMean());
//    }
//
    public Double cToF() throws JRScriptletException {
    	Double fahrenheit = ((1.8*valueFromParam) + 32);
    	return fahrenheit;
    }

    public String getMd5(String s) throws NoSuchAlgorithmException {    	
    	md5.update(StandardCharsets.UTF_8.encode(s));
    	return String.format("%032x", new BigInteger(1, md5.digest()));
    }


    
//    public String getAttestazioneVal(String xml, String name) throws JRScriptletException, Exception {
//    	if (xml == null) {
//    		return "";
//    	}   	
//    	DichiarazioneRedditualeBean dichiarazione = getAttestazione(xml);    	
//    	return dichiarazione.toString();
//    }
//    
//    
//    
//    final static LocalDate getLocalDate(String str) {
//    	return new LocalDate(DichiarazioneRedditualeBean.fmtYYYYMMDD2.parseMillis(str)); 
//    }
//
}





