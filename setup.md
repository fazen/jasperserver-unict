# Installazione e configurazione del server Jasperserver

Tra le varie opzioni, è possibile installare e usare il programma in locale ([installazione che include tomcat e postgres](https://community.jaspersoft.com/project/jasperreports-server/releases)) o utilizzare e configurare una [macchina virtuale predisposta da bitnami](https://bitnami.com/stack/jasperreports):

Variabili di questo documento:

- `%JAVA_DIR%` -- il path di installazione di Java
- `%TOMCAT_DIR%` -- il path di installazione di Tomcat
- `%JR_DIR%` -- il path di installazione di Jasperserver
- `%JR_SERVER%` -- il server che ospita il Jasperserver (ad es. `192.168.122.74`)

## 1. Installazione attraverso immagine Bitnami

Comandi per interagire con jasperserver:
```
cat bitnami_credentials
sudo /opt/bitnami/ctlscript.sh start|stop|restart
sudo /opt/bitnami/ctlscript.sh start|stop|restart tomcat
```

### Riferimenti

- [Bitmani Jasperserver stack](https://bitnami.com/stack/jasperreports)
- [Converting OVA for use with KVM / QCOW2](https://edoceo.com/notabene/ova-to-vmdk-to-qcow2) (Convertire immagine (ova) per Qemu)

### Problemi e soluzioni

#### Errore nella generazione di ogni report

Usando l'immagine di Bitnami, il server non generava i report (neanche quelli di esempio).

Controllando le eccezioni evidenziate dai log (`/opt/bitnami/apache-tomcat/webapps/jasperserver/WEB-INF/logs/jasperserver.log`) ho risolto con:

- `sudo apt-get install ttf-dejavu` (non so se effettivamente serve)
- `sudo apt install fontconfig`

## 2. Modifiche all'installazione base

### Creazione e abilitazione certificato ssl per il server

Nell'immagine bitnami esiste già un certificato (impostato in Apache2), non è necessario impostare anche quello del Tomcat.

### Configurazione utenti tomcat (opzionale)

...

## 3. Integrazione di jasperserver con il CAS e ruoli recuperati dal database

### Certificato CAS

Aggiungo i certificati del server cas.unict.it al keystore java:

- `sudo keytool -importcert -alias cascert -keystore %JAVA_DIR%/lib/security/cacerts -file cas_unict_it.crt`
- `sudo keytool -importcert -alias cascert_root -keystore %JAVA_DIR%/lib/security/cacerts -file DigiCertCA.crt`

### Configurazione Webapp

Utilizzo il modello di file `sample-applicationContext-externalAuth-CAS-db-staticRoles.xml` che si trova in `%JR_DIR%/samples/externalAuth-sample-config/`.

Lo copio in `%TOMCAT_DIR%/webapps/jasperserver/WEB-INF/` rinominandolo come `applicationContext-externalAuth-CAS-db-staticRoles.xml`.

Copio il driver per SQLServer (`sqljdbc42.jar`) nella cartella `%TOMCAT_DIR%/lib` e aggiorno il file:

```
<bean id="casRestServiceProperties" class="com.jaspersoft.jasperserver.api.security.externalAuth.wrappers.spring.cas.JSCASServiceProperties">
    <property name="service" value="https://%JR_SERVER%/jasperserver/rest_v2/login"/>
    <property name="sendRenew" value="false"/>
</bean>

<bean id="casJDBCUserDetailsService" class="com.jaspersoft.jasperserver.api.security.externalAuth.cas.CasJDBCUserDetailsService">
    <property name="dataSource" ref="externalDataSource"/>
    <property name="usersByUsernameQuery" value="select CodiceFiscale username, 'xxx' cognome, '1' attivo from StruttureDidattiche_Organigrammi_Ruoli where CodiceFiscale=?"/>
    <property name="authoritiesByUsernameQuery" value="select CodiceFiscale username, Ruolo rolename from StruttureDidattiche_Organigrammi_Ruoli where CodiceFiscale=?"/>
</bean>
```
...
```
<bean id="casServiceProperties" class="com.jaspersoft.jasperserver.api.security.externalAuth.wrappers.spring.cas.JSCASServiceProperties">
    <property name="service" value="https://%JR_SERVER%/jasperserver/j_spring_security_check"/>
    <property name="sendRenew" value="false"/>
</bean>
```
... (gli utenti che saranno mappati al ruolo admin di Jasperserver)
```
<property name="adminUsernames">
    <list>
        <value>MRTSFN74D09F943X</value>
    </list>
</property>
```
...
```
<property name="ssoServerLocation" value="https://cas.unict.it/cas"/>
```
...

### External DB

Per l'utilizzo di un "External DB" si potrebbero inserire i parametri di connessione in un file generale dedicato (`master.properties`?). Per adesso ho impostato la connessione direttamente in `externalAuth-CAS-db-staticRoles.xml`:

```
<bean id="externalDataSource" class="com.jaspersoft.jasperserver.api.security.externalAuth.wrappers.spring.jdbc.JSDriverManagerDataSource">
    <property name="driverClassName" value="com.microsoft.sqlserver.jdbc.SQLServerDriver"/>
<property name="url" value="jdbc:sqlserver://151.97.242.13:1433;databaseName=SMARTEDU_UNICT_PRODUZIONE"/>
    <property name="username" value="unict.webateneo"/>
    <property name="password" value="***"/>
</bean>
```

### Riferimenti

- [Documentazione per integrare le utenze con un CAS](https://community.jaspersoft.com/documentation/tibco-jasperreports-server-authentication-cookbook/v720/cas-authentication)

## 4. Da fare

- Configurazione posta in uscita
- Certificato locale del server
- Verifica funzionalità REST
- ...

