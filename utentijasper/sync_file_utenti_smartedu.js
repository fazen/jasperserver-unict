const fs = require('fs');
const JDBC = require('jdbc');
const jinst = require('jdbc/lib/jinst');
const asyncjs = require('async');
// const date_fns = require('date-fns')
//import { format, formatDistance, formatRelative, subDays } from 'date-fns'

const conf = require('dotenv').config()

if (!jinst.isJvmCreated()) {
  jinst.addOption("-Xrs");
  jinst.setupClasspath(['./drivers/sqljdbc42.jar']);
}

var config = {
  // Required
  //jdbc:sqlserver://151.97.242.13:1433;databaseName=SMARTEDU_UNICT_PRODUZIONE
  url: `jdbc:sqlserver://${process.env.DB_HOST};databaseName=SMARTEDU_UNICT_PRODUZIONE`,

  // Optional
  drivername: 'com.microsoft.sqlserver.jdbc.SQLServerDriver',
  minpoolsize: 10,
  maxpoolsize: 100,

  // Note that if you sepecify the user and password as below, they get
  // converted to properties and submitted to getConnection that way.  That
  // means that if your driver doesn't support the 'user' and 'password'
  // properties this will not work.  You will have to supply the appropriate
  // values in the properties object instead.
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  properties: {}
};

// or user/password in url
// var config = {
//   // Required
//   url: 'jdbc:hsqldb:hsql://localhost/xdb;user=SA;password=',
//
//   // Optional
//   drivername: 'my.jdbc.DriverName',
//   minpoolsize: 10
//   maxpoolsize: 100,
//   properties: {}
// };

// or user/password in properties
// var config = {
//   // Required
//   url: 'jdbc:hsqldb:hsql://localhost/xdb',
//
//   // Optional
//   drivername: 'my.jdbc.DriverName',
//   minpoolsize: 10,
//   maxpoolsize: 100,
//   properties: {
//     user: 'SA',
//     password: ''
//     // Other driver supported properties can be added here as well.
//   }
// };

var hsqldb = new JDBC(config);

hsqldb.initialize(function(err) {
  if (err) {
    console.log(err);
  }
});



const storeData = (data, path) => {
  try {
    fs.writeFileSync(path, JSON.stringify(data, null, 2))
  } catch (err) {
    console.error(err)
  }
}

hsqldb.reserve(function(err, connObj) {
  // The connection returned from the pool is an object with two fields
  // {uuid: <uuid>, conn: <Connection>}
  if (connObj) {
    console.log("Using connection: " + connObj.uuid);
    // Grab the Connection for use.
    var conn = connObj.conn;

    // Adjust some connection options.  See connection.js for a full set of
    // supported methods.
    asyncjs.series([
      function(callback) {
        conn.setAutoCommit(false, function(err) {
          if (err) {
            callback(err);
          } else {
            callback(null);
          }
        });
      },
      function(callback) {
        conn.setSchema("test", function(err) {
          if (err) {
            callback(err);
          } else {
            callback(null);
          }
        });
      }
    ], function(err, results) {
      // Check for errors if need be.
      // results is an array.
    });

    // Select statement example.
    conn.createStatement(function(err, statement) {
      if (err) {
        callback(err);
      } else {
        let userInfoArray = [];
        statement.executeQuery(
          `select uid, cognome, nome, logonname, email
	FROM UsersAccounts g WITH(NOLOCK)
	WHERE
	cognome is not null
	and trim(cognome) <> ''
	and logonname is not null
	and logonname like '[A-Z][A-Z][A-Z][A-Z][A-Z][A-Z][0-9][0-9]________'
	and g.ObjectType <> 'GompGroup'
	ORDER BY g.Denominazione;
          `, function(err, resultset) {
          if (err) {
            callback(err)
          } else {
            // Convert the result set to an object array.
            resultset.toObjArray(function(err, results) {
              //console.log(results)
              if (results.length > 0) {
                results.forEach(row => {
                  var user = {
                    uid: row.uid,
                    cf: row.logonname,
                    nomeCompleto: `${row.cognome} ${row.nome}`,
                    email: row.email
                  }
                  userInfoArray.push(user)
                })
              }
              //callback(null, resultset);
              console.log(userInfoArray);
              // salvo il file
              let isoTime = (new Date()).toISOString().slice(0,10)
              let backupFilename = `./backup/utenti_smartedu_all2_${isoTime}.json`
              // faccio un backup
              fs.copyFile('./utenti_smartedu_all2.json', backupFilename, (err) => {
                if (err) throw err;
                console.log(`Backup effettuato (${backupFilename})`);
              })
              storeData(userInfoArray, './utenti_smartedu_all2.json')

            });
          }
        });


      }
    });

  }
});
