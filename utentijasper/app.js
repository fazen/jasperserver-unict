const fs = require('fs')
const request = require("request")
const _ = require("lodash")
const conf = require('dotenv').config()

const curlOptions = {
  // method: 'GET',
  //url: 'https://reportdidattica.unict.it/jasperserver/rest_v2/users/MRTSFN74D09F943X',
  headers:
   { 'cache-control': 'no-cache',
     Connection: 'keep-alive',
     //'content-length': '35',
     'accept-encoding': 'gzip, deflate',
     cookie:  'userLocale=it_IT',
     Host: 'reportdidattica.unict.it',
     'Cache-Control': 'no-cache',
     'User-Agent': 'UnictScript/0.1',
     Authorization: process.env.REST_AUTH,
     'Content-Type': 'application/json',
     accept: 'application/json' },
  json: true
}

const isCF = (stringa) => {
  return stringa.match(/^[A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z0-9]{5}$/)
}

const dryRun = process.env.DRY_RUN

/*
Altre possibilità per arguments:

- search  Specify a string or substring to match the user ID or full name of any user. The search is not case sensitive.
- requiredRole  Specify a role name to list only users with this role. Repeat this argument
to filter with multiple roles. (può essere un array di ruoli)
- hasAllRequiredRoles When set to false with multiple requiredRole arguments, users will match if they have any of the given roles (OR operation). When true or not specified, users must match all of the given roles (AND operation).
*/
const fetchRDUsers = (arguments, callback) => {
  if (!arguments) {
    console.log("arguments non definito")
    return
  }

  const options = {
    ...curlOptions,
    method: 'GET',
    url: `${process.env.REST_BASEURL}/users`,
    qs: arguments,
    qsStringifyOptions: { arrayFormat: 'repeat' } // per gestire requiredRole multipli
  }

  //console.log(JSON.stringify(options, undefined, 2))

  request(options, function(error, response, body) {
    if (error) throw new Error(error)
    //console.log(body)
    callback(body && body.user)
    return body && body.user
  })

}

/*
Aggiorna il nome completo (fullName) degli utenti forniti.
Se "force" allora aggiorna tutti anche se il fullName è diverso dal solo codice fiscale.
*/
const updateRDUsers = (force = false) => {
  // l'utente deve appartenere ad almeno uno dei "requiredRole".
  fetchRDUsers({
    requiredRole: [
      'PRESIDENTEDELCORSODISTUDIO_EXTERNAL',
      'UFFICIO_ANAGRAFE_REDDITUALE',
      'TECNICOAMMINISTRATIVO'
    ],
    hasAllRequiredRoles: false
  }, (utenti) => {

    if (!utenti) {
      console.log('Nessun utente restituito')
      return
    }
    let utentiEsterniAttivi = utenti.filter( user => (user.externallyDefined && user.enabled) )
    //console.log(JSON.stringify(utentiEsterniAttivi, undefined, 2))
    console.log(`Restituiti ${utentiEsterniAttivi.length} utenti`)

    fs.readFile('./utenti_smartedu_all2.json', 'utf8', function(err, data) {

      if (err) throw err

      let userInfos = JSON.parse(data)
      //console.log(userInfos)

      utentiEsterniAttivi.forEach(user => {

        if ( isCF(user.username) && (force || isCF(user.fullName)) ) {
          let cf = user.username
          //console.log(`Utente: ${user.fullName}`)
          let userInfo = (userInfos.filter((u) => (u.cf === cf)))[0]
          if (userInfo) {
            //console.log(userInfo)
            //console.log("UPD")
            let nomeCompleto = _.startCase(_.lowerCase(`${userInfo.nomeCompleto}`)) + ` (${cf})`
            //console.log(cf, nomeCompleto)

            if (dryRun) {
              console.log(`Aggiornerei ${cf} come: "${nomeCompleto}"`)
            }
            else {
              // Aggiorno sul server in nomeCompleto di cf
              console.log(`Aggiorno ${cf} come: "${nomeCompleto}"`)
              const options = {
                ...curlOptions,
                method: 'PUT',
                url: `${process.env.REST_BASEURL}/users/${cf}`,
                body: { fullName: `${nomeCompleto}` }
              }
              request(options, function(error, response, body) {
                if (error) throw new Error(error)
                console.log(body)
              })
            }
          }
        }
      })
    })
  })
}


// fetchRDUsers({ requiredRole: ['PRESIDENTEDELCORSODISTUDIO', 'TECNICOAMMINISTRATIVO'] }, (utenti) => {
//
//   //let utentiEsterniAttivi = utenti.filter(user => { return (user.externallyDefined && user.enabled) })
//   let utentiEsterniAttivi = utenti.filter(user => (user.externallyDefined && user.enabled) )
//
//   utentiEsterniAttivi.forEach(user => {
//     console.log(`Utente: ${user.username}`)
//     console.log(user)
//
//     if (isCF(user.fullName)) {
//       console.log(`fullName: ***fullName da aggiornare***`)
//     }
//     else {
//       console.log(`fullName: ${user.fullName}`)
//     }
//     let ruoliesterni = user.roles.filter(r => r.externallyDefined).map(r => r.name)
//     ruoliesterni.length && console.log(`Ruoli esterni: ${ruoliesterni}`)
//     console.log(`---`)
//   })
//
// })
// fetchRDUsers()
// fetchRDUsers({ search: 'drago' })


updateRDUsers()
